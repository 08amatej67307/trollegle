system | %2$s
normal [%s] %s
admin %2$s
special [(%3$s) %1$s] %2$s
action  * %s %s

captcha We've been captcha'd! No new users can join, unless the admin manually solves captchas. All you can do is watch the room die.
captchapublic We've been captcha'd! No new users can join, unless a kind soul manually solves captchas. All you can do is watch the room die.
hardban We've been hard banned! No new users can join. Have fun watching the chat die off.
captchawin Captcha submitted, thank you!
captchalose Couldn't parse captcha response.

left %s has left
leftreason %s has left (%s)
onlyleft You are the only one left. We'll try to get you another stranger.

toothpaste (?i)\btoothpaste\b
toothpasteprompt the word "toothpaste"
saytoothpaste To enter the room, you need to say %s.
rulelist Every group will have its own norms, so I'm not posting any hard and fast rules here. If you're not sure if some behaviour is halaal, you can hold off until you've lurked enough. Or ask around even. If someone is bothering you, why not talk it out? There's also the /kick command as a last resort.

help help
help_alt h ? commands

invite invite
invite_alt iq ic ii
invitedq %s has invited a new user
invitedc %s has invited a new user (requesting toothpaste - hold on)
invitetoofast Too fast! Try again in a few seconds.
invitefull Chat room is full.

pm msg
pm_alt pm bug msgid privmsg private whisper dm
pm_usage USER MESSAGE
pm_help Sends a private message to the user
pmnouser No such user - type /list to see who's online.
pmlurker You can't talk to non-lurkers as a lurker.
nousershort No such user
nousers Some users couldn't be found; ignoring them
pmsent message sent

me me
me_alt action self
me_usage MESSAGE
me_help Sends an irc-like action message

nick nick
nick_alt name n
nick_usage NAME
nick_help Changes your name to the specifed NAME
nickdigits Your name can't consist of nothing but digits.
nickchange '%s' is now known as '%s'
silentnickchange You are now known as '%s'
nicktaken Someone already has that name. Try another one.

list list
list_alt who why users
list_help Shows who's in the chat
listhead There are %d users in the room:
listitem %d | %s %s
listyou <- you
listignored 🛇 
listlurker 👻

spam spam
spam_alt souls s spamon
spam_usage or /nospam
spam_help Starts/stops looking for new users (one every %d s, currently %s)
spam_help_on on
spam_help_off off
nospam nospam
nospam_alt spamoff nosouls dontspam ns nos
spambanned Spamming is off because the room is banned.
spampausedfull Spamming is paused because the room is full (max %d users). It will resume once someone leaves.
spampausedlingering Spamming is paused because of lingering connections. It will resume within %d seconds.
spamalreadyon Spamming is already on. Current rate is %d seconds.
spamalreadyoff Spamming is already off.
spamon %s has turned spamming on.
spamoff %s has turned spamming off.

kick kick
kick_alt hurt ban
kick_usage USER or /nokick USER
kick_help Votes for the user to be kicked (2/3 of the room needs to do this for the kick to happen)
kicknouser No user exists with that name. Maybe they left of their own accord?
kicktooyoung You aren't old enough to vote yet. Wait a minute or two.
kickvote %s has voted to kick %s from the room.

dontkick nokick
dontkick_alt dontkick unkick nohurt donthurt unhurt unban dontban noban
dontkick_usage USER or /kick USER
dontkicknouser No user exists with that name, maybe they're already gone
dontkickvote %s has withdrawn their vote to kick %s from the room.

pat pat
pat_alt pats pet
pat_usage USER
patuser %1$s pats %2$s (pat count: %3$d)
noselfpat No free pats allowed!
pattoofast You're patting too fast!

heyadmin heyadmin
heyadmin_alt fuckyoubecause admin summoningritual gently
heyadmin_usage MESSAGE
heyadmin_help Sends a message to the administrator of this system
admincalled %s has called the admin. Might be away from the keyboard though, in which case tough luck.

nocommand That isn't a known command. Type /help for more info.
commanddisabled This command is currently disabled.

joinedtext %s has joined
joinedquestion %s has joined under question: '%s'

kickinactive Inactive for too long
kickspam Their message looked like spam
kickvoted The room voted to kick them
kickflood Kicked for flooding. Limit is %d messages in %s seconds
kickadmin Kicked by admin
taileat Kicked for possible tail-eating

ignore ignore
ignore_alt block mute
ignore_usage USER or /unignore USER
ignore_help Adds or removes a user to or from your ignore list
unignore unignore
unignore_alt unblock unmute dontignore dontblock dontmute
unignore_usage USER or /ignore USER
ignorelist ignorelist
ignorelist_alt blocklist mutelist ignored blocked muted
clearignore clearignore
clearignore_alt clearignores clearblock clearblocks clearmute clearmutes unignoreall unblockall unmuteall
clearignore_help Removes all users from your ignore list

cannotignore You cannot add yourself or the room host to your ignore list.
addignore %s has been added to your ignore list.
alreadyignored %s is already on your ignore list.
notignored %s is not on your ignore list.
removeignore %s has been removed from your ignore list.
ignorecleared Your ignore list has been cleared!
ignoreheadone There is %s user on your ignore list:
ignorehead There are %s users on your ignore list:
ignoreempty Your ignore list is currently empty

nolurk nolurk
nolurk_alt unlurk
lurk lurk
lurk_alt relurk
lurk_NORMAL You are no longer marked as a lurker.
lurk_PRELURKER You have been inactive for a while and will soon be marked as a lurker. If you'd like to stay as a normal user, start talking or type /nolurk.
lurk_LURKER You have been marked as a lurker. You can return as a normal user by typing /nolurk
lurkhelp Lurkers don't appear on the user list and their messages, marked with a 👻, are only visible to other lurkers as well as admins.
lurk_admin_NORMAL %s is no longer a lurker
lurk_admin_PRELURKER %s has been informed about lurker mode
lurk_admin_LURKER %s has been marked as a lurker
lurk_admin_manual %s has been manually marked as a lurker
lurk_reset You will be marked as a lurker after %s of inactivity.
lurk_already_lurking You are already lurking.
lurk_already_sticky You are already immune to lurking.
lurk_sticky You are now immune to lurking.

softban Your last message was caught by the spam filter.
softbancomment Your last message was caught by the spam filter: %s
warnban Your last message was caught by the spam filter. You will be kicked if you trip it again!
warnbancomment Your last message was caught by the spam filter: %s. You will be kicked if you trip it again!
private private
lurkprefix 👻 

id id
idreply You: %s\nPulse: %s\nChat name: %s on mothership: %s
pulseid %s (%s, %s, %s old)
idnopulse none

8 8
8reply You've been here for %s. /8 count: %d

age %s old
zombieage waiting for %s
waitingage listening for %s
unconfirmedage connected %s ago
textmode text mode
questionmode question mode
defaulttopics default topics

rules rules

showids showids
showidstwo showidstwo
showids_alt duids dids
showidstwo_alt dids2 duids2
hideids hideids
hideids_alt huids hids hideidstwo hids2 huids2
bot bot
unbot unbot
unbot_alt nobot

wrongpassword Wrong password.
logbotwelcome Welcome back!
logboterror /LogBot error for %1$s: %2$s
floodwarning The flood limit is %d messages in %s seconds. %s, you will be kicked if you continue.
floodtoofast Your last message was not sent due to flood control restrictions: currently one message every %s seconds.
chatover This chat is over!
usage Usage: %s
sendcodes Curious about how this works? Download teh codes at https://gitlab.com/jtrygva/trollegle
rejoin Want to leave and rejoin later? See %s

stumbledLine youHave|stumbled|roving|groupChat
flauntLine doNote|notThePlace|flauntGenitalia|orAnything
emptyLine nobody|so|boring
youHaveEntered youHave|entered
youHaveEnteredSingle This is |You're in 
rovingGroupChat roving|groupChat

youHave You've |You have 
stumbled stumbled upon |come upon |been chosen to take part in |been selected to enter 
entered stumbled upon |come upon |entered |found 
roving a roving |a wandering |a |an Omegle 
groupChat group chat. |groupchat. |multi-user chat. |chat room. |chatroom. 
doNote Do note, |Note that |Please be aware that |Do be aware that 
notThePlace this is not the place to |this is not a place to 
flauntGenitalia flaunt the fitness of your sex organs|brag about your sexual prowess|advertise your physical attractiveness|display your undoubtedly amazing genitalia
orAnything  or somesuch. | or anything. | or anything like that. |. 
sayToothpasteToEnter To enter the __room__, you need to say %s. |Type %s to enter the __room__. |Enter the __room__ by typing %s. |To enter the __room__, say %s as a show of human conformity. |To enter the __room__, say %s. 
room room|chat room|chatroom|chat
nobody No one else has entered the __room__ yet|The __room__ is still empty|Nobody else has joined yet
so  so |, so | - 
boring be prepared for a bore.|you may need to wait a while.|you'll need to be patient.

firstUserLine youAre|firstUser|inTheRoom|holdOn|lookFor|someoneElse

youAre You are |You happen to be |You're 
firstUser the first one |the first user 
inTheRoom in the __room__. |in this __room__. |here. |to join. 
holdOn Hold on|You'll have to wait a while
lookFor |we're looking for | while we look for ||we're trying to find 
someoneElse someone else. \n|another user. \n|more users. \n|another stranger. \n

normalUsername '%s' is your username. Everyone sees it in front of the messages you send.
emptyRoomUsername (Your username will be: '%s')
shortHelp Type '/nick NAME' to change your name, or '/help' to see the commands you can use.

nOthers There are __num__ __others__ in the __room__|__num__ __others__ are in the __room__
others users|others|other users|other strangers|other losers
