package anon.trollegle;

import java.io.*;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Locale;
import java.util.function.ObjIntConsumer;

import static anon.trollegle.Util.t;

public class AdminCommands extends Commands {

    public AdminCommands(Multi m) {
        super(m);
    }
    
    protected void addAll() {

        addCommand("help", null, null, 0,
            (args, target) -> {
                String search = args.length == 0 || args[0].equals("full") 
                    ? null : argsToString(0, args).toLowerCase(Locale.ROOT);
                StringBuilder sb = new StringBuilder(args.length > 0 ? "Showing admin command help:"
                        : "Showing list of admin commands. Say '/!help full' for more detail");
                int columnSize = 2 + Collections.max(commands.values(), new Comparator<Command>() {
                    public int compare(Command a, Command b) {
                        return a.usage.length() - b.usage.length();
                    }
                }).usage.length();
                StringBuilder othersb = new StringBuilder();

                for (Command c : commands.values()) {
                    String helpstring = c.getHelpString(target);
                    if (helpstring != null) {   // use null helpstring to hide a command
                        if (search != null 
                                && !helpstring.toLowerCase(Locale.ROOT).contains(search)
                                && !c.usage.toLowerCase(Locale.ROOT).contains(search)
                                && commands.get(search) != c
                                && aliases.get(search) != c)
                            continue;
                        if (args.length > 0 || c.helpstring == null) {
                            sb.append("\n").append(c.usage);
                            for (int pad = c.usage.length(); pad < columnSize; pad++)
                                sb.append(" ");
                            sb.append(helpstring);
                        } else {
                            othersb.append(c.usage).append("   ");
                        }
                    }
                }
                sb.append("\nTo see muted and private messages, type /v (/t to stop)");
                if (othersb.length() > 0)
                    sb.append("\nOther commands: ").append(othersb);
                else if (search == null)
                    sb.append("\n\n——\u2003Thanks to Spacecat for cleaning house!");

                target.schedTell(sb.toString());
            });

        addCommand("broad", "broad MESSAGE", "Say something (only useful on console)", 0,
                (args, target) -> m.hear(target, argsToString(0, args)),
                "b");
        
        addCommand("broadmute", "broadmute MESSAGE", "Say something to muted people", 0,
                (args, target) -> m.sendToMuted(target, argsToString(0, args), false),
                "bm");

        addCommand("announce", "announce MESSAGE", "Fake a system message. Doot doot!", 0,
                (args, target) -> m.tellRoom(argsToString(0, args)),
                "a", "ann", "anno");
        
        addCommand("annmute", "annmute MESSAGE", "Fake a system message for muted people", 0,
                (args, target) -> m.tellMuted(argsToString(0, args)),
                "annm", "am", "amute");
        
        addCommand("annlurk", "annlurk MESSAGE", "Fake a system message for lurkers", 0,
                (args, target) -> m.tellLurkers(argsToString(0, args)),
                "annl", "al", "alurk");
        
        addCommand("annpm", "annpm USER MESSAGE", "Fake a system message for the specified user", 0,
                (args, target) -> m.annPrivate(target, args[0], argsToString(1, args)),
                "annp", "ap", "apm", "annprivate", "annpriv", "apriv", "annmsg", "amsg");
        
        addCommand("gossip", "gossip MESSAGE", "Gossip among the admins", 0,
                (args, target) -> m.tellRoom(to -> to.schedSend(t("special", m.getDisplayNick(to, target), argsToString(0, args), "goss")),
                        to -> to.isReady() && m.isAdmin(to) && (to.isDummy() || to != target)),
                "goss", "g", "modchat", "mchat", "mc", "c");
        
        addCommand("adminmessage", null, null, 0,
                (args, target) -> { if (target.isDummy()) m.tellAdmin(argsToString(0, args)); });

        addCommand("kick", "kick PERSON...", "Kick that cheeky motherfucker! >:o", 1,
                forEachCommand(arg -> m.kick(arg, t("kickadmin"))),
                "kickid", "k");

        addCommand("customkick", "customkick PERSON [REASON]", "Kick with a custom reason (or none)", 1,
                (args, target) -> expectUser(target, m.kick(args[0], args.length > 1 ? argsToString(1, args) : null)),
                "ckick", "ck", "qkick", "qk", "kiq");

        addCommand("mute", "mute|unmute PERSON...", "Make them shut up for reals!", 1,
                forEachCommand(m::mute),
                "muteid", "m");
        
        addCommand("unmute", null, null, 1,
                forEachCommand(m::unmute),
                "um");
        
        addSetting("showmuted", "(show|hide)muted",
                target -> "Show or hide muted users on the list (currently " 
                    + (m.countMuted ? "shown" : "hidden") + ")", 0,
                (args, target) -> { m.countMuted = true; },
                "showmute", "showmutes");

        addCommand("hidemuted", null, null, 0,
                (args, target) -> { m.countMuted = false; },
                "hidemute", "hidemutes");

        addCommand("setlurk", "setlurk (default|on|off) PERSON...", "Set the user's lurk state", 2,
            (args, target) -> {
                Boolean state;
                if ("on".equals(args[0])) {
                    state = true;
                } else if ("off".equals(args[0])) {
                    state = false;
                } else if ("default".equals(args[0])) {
                    state = null;
                } else {
                    target.schedTell("The state needs to be 'default', 'on', or 'off'");
                    return;
                }
                String[] result = Util.filter(Arrays.copyOfRange(args, 1, args.length), 
                    arg -> {
                        MultiUser u = m.userFromName(arg);
                        if (u == null)
                            return false;
                        u.setLurkPreference(state);
                        return true;
                    });
                if (result.length < args.length - 1)
                    target.schedTell(t("nousers"));
            });
        
        addSetting("showlurkchanges", "(show|hide)lurkchanges", 
                target -> "Show or hide join/leave messages for lurk state changes (currently "
                    + (m.showLurkChanges ? "shown" : "hidden") + ")", 0,
                (args, target) -> { m.showLurkChanges = true; });

        addCommand("hidelurkchanges", null, null, 0,
                (args, target) -> { m.showLurkChanges = false; });
        
        addCommand("showjoin", "show(join|leave) PERSON [REASON]", "Fake a join or leave message for the user", 1,
                (args, target) -> expectUser(target, m.showJoin(args[0])),
                "fakejoin", "showjoined", "fakejoined", "sj");
        
        addCommand("showleave", null, null, 1,
                (args, target) -> expectUser(target, m.showLeave(args[0], args.length > 1 ? argsToString(1, args) : null)),
                "fakeleave", "showleft", "fakeleft", "sl");
        
        addCommand("clearignorelists", null, "Remove all ignore lists", 0,
                (args, target) -> m.clearAllIgnore(),
                "clearallignore");

        addSetting("invite", null,
                target -> "Invite a user, " + m.qfreq + " from question mode", 0,
                (args, target) -> m.add(false, args),
                "i");
            
        addCommand("qfreq", "qfreq DECIMAL", "Set the question mode fraction to FREQ", 1,
            (args, target) -> {
                double f = Double.parseDouble(args[0]);
                if (f >= 0 && f <= 1) {
                    m.qfreq = f;
                } else {
                    target.schedTell("Must be between 0 and 1");
                }
            });

        addCommand("invitep", "invite(p|q|c)", "Invite a returning (\"pulse\"), question mode, or chat mode user, respectively", 0,
                (args, target) -> m.addPulse());

        addCommand("inviteq", null, null, 0,
                (args, target) -> m.add(true, args));

        addCommand("invitec", null, null, 0,
                (args, target) -> m.add(null, args));

        addCommand("list", "list [trivia] [proxies]", "List all users (/!flaghelp to see meanings of flags)", 0,
            (args, target) -> {
                String arg = argsToString(0, args).toLowerCase();
                showList(target, arg.contains("t"), arg.contains("p"));
            }, "flist", "who");
        
        addCommand("idlist", null, null, 0, (args, target) -> showList(target, true, true));
        
        addCommand("flaghelp", null, null, 0,
            (args, target) -> {
                target.schedTell("These flags may follow a user's name in the /!list:"
                        + "\nA: admin"
                        + "\nC: captcha'd"
                        + "\nD: ignores you"
                        + "\nI: ignored by you"
                        + "\nL: LogBot"
                        + "\nM: muted"
                        + "\nP: listed pulse"
                        + "\nU: has yet to type the entry word"
                        + "\nT: immune to lurker marking"
                        + "\nW: user not yet connected"
                        + "\nZ: session not yet created"
                        + "\n8: lurker");
                target.schedTell("Passing \"trivia\" shows a user's age and how they joined; \"proxies\" shows which proxy they're on.");
            });
        
        addCommand("freqconst", new Command("freqconst FREQ", null) {
            @Override
            public void process(String[] args, MultiUser target) {
                long f = Util.parseTime(args[0]);
                if (f >= 0) {
                    m.joinFreqConstant = f;
                } else {
                    target.schedTell("Must be 0 or greater");
                }
            }

            @Override
            public String getHelpString(MultiUser target) {
                return "Set the spam frequency constant to FREQ (currently " + Util.minSec(m.joinFreqConstant) + ")";
            }
        });

        addCommand("freq", new Command("freq FREQ", null) {
            @Override
            public void process(String[] args, MultiUser target) {
                long f = Util.parseTime(args[0]);
                if (f >= 250) {
                    m.joinFreq = f;
                } else {
                    target.schedTell("Must be 0.25s or greater (did you forget a unit?)");
                }
            }

            @Override
            public String getHelpString(MultiUser target) {
                return "Set the spam frequency factor to FREQ (currently " + Util.minSec(m.joinFreq) + ")";
            }
        });

        addSetting("pfreq", "pfreq FREQ",
                target -> "Set the pulse frequency to FREQ (-1 to disable, currently " + Util.minSec(m.pulseFreq, "disabled") + ")", 1,
                (args, target) -> { m.pulseFreq = Util.parseTime(args[0]); });
        
        addSetting("pausepulse", "pausepulse TIME",
                target -> "Temporarily disable pulses for TIME (0 to resume; " + (m.getPulseCooldown() > 0 ? 
                    "currently paused for " + Util.minSec(m.getPulseCooldown()) : "not currently paused") + ")", 1,
                (args, target) -> m.pausePulse(Util.parseTime(args[0])),
                "disablepulse");
        
        addCommand("resumepulse", null, null, 0, (args, target) -> m.pausePulse(0));
        
        addCommand("topics", "topics TOPIC...", "View or replace the default topics for text mode users", 0,
            (args, target) -> {
                if (args.length == 0)
                    target.schedTell(UserConnection.getTopicList());
                else
                    UserConnection.setDefaultTopics(args);
            });
        
        addCommand("addtopics", "(add|rm)topics TOPIC...", "Add or remove default topics for text mode users", 1,
                (args, target) -> UserConnection.addTopics(args),
                "addtopic");
        
        addCommand("removetopics", "removetopics TOPIC...", null, 1,
                (args, target) -> UserConnection.removeTopics(args),
                "rmtopics", "removetopic", "rmtopic");
        
        addCommand("spam", null, null, 0, (args, target) -> m.spam(target, true), "s");
        addCommand("nospam", null, null, 0, (args, target) -> m.nospam(target, true), "nos", "ns");
        
        addSetting("toothpastepattern", "toothpastepattern REGEX",
                target -> "Set the entry password (currently: " + m.getEntryPattern() + ")", 1,
                (args, target) -> m.setEntryPattern(argsToString(0, args)), "entrypattern");
        
        addSetting("toothpasteprompt", "toothpasteprompt TEXT",
                target -> "Set the entry password prompt (currently: " + m.getEntryPrompt() + ")", 1,
                (args, target) -> m.setEntryPrompt(argsToString(0, args)), "entryprompt");
        
        addCommand("toothpastefor", new Command("toothpastefor TYPE...", null) {
            @Override
            public void process(String[] args, MultiUser target) {
                synchronized (m.toothpasteTypes) {
                    m.toothpasteTypes.clear();
                    for (String arg : args)
                        m.toothpasteTypes.add(Multi.JoinType.valueOf(arg.toUpperCase(Locale.ROOT)));
                }
            }
            
            @Override
            public String getHelpString(MultiUser target) {
                return "Set which kinds of user need to type the entry password (text, question, or pulse; currently "
                    + m.toothpasteTypes.toString().toLowerCase() + ")";
            }
        }, "entryfor");
        
        addSetting("maxchatting", "maxchatting LIMIT",
                target -> "Set the desired number of users per proxy (currently " + m.maxChatting + ")", 1,
                (args, target) -> { m.maxChatting = Integer.parseInt(args[0]); });

        addSetting("maxtotal", "maxtotal LIMIT",
                target -> "Set the maximum number of connections per proxy (currently " + m.maxTotal + ")", 1,
                (args, target) -> { m.maxTotal = Integer.parseInt(args[0]); });

        addSetting("verbose", "verbose|terse", 
                target -> "Verbose output, including system messages sent to users (currently "
                    + (UserConnection.verbose ? "verbose" : "terse") + ")", 0,
                (args, target) -> { if (target.isDummy()) UserConnection.verbose = true; });

        addCommand("terse", null, null, 0,
                (args, target) -> { if (target.isDummy()) UserConnection.verbose = false; });

        addSetting("murder", "revive|murder",
                target -> "Resume spamming when the room empties out (currently "
                    + (m.murder ? "murder-happy" : "revive-happy") + ")", 0,
                (args, target) -> { m.murder = true; });

        addCommand("revive", null, null, 0,
                (args, target) -> { m.murder = false; });

        addCommand("flood", "flood COUNT TIME", "Kick users who send more than COUNT messages per TIME", 0,
            (args, target) -> {
                if (args.length >= 2) {
                    if (args[0].matches("[0-9]+")) {
                        m.floodKickSize = Integer.parseInt(args[0]);
                    }
                    if (args[1].matches("[\\-0-9].*")) {
                        m.floodKickTime = Util.parseTime(args[1]);
                    }
                }
                target.schedTell("Flood limit: " + m.floodKickSize + " messages in " + Util.minSec(m.floodKickTime));
            });

        addSetting("floodcontrol", "floodcontrol TIME",
                target -> "Set the minimum amount of time a user must wait between sending messages to TIME (-1 for no limit, currently " + Util.minSec(m.floodControlTime, "no limit") + ")", 1,
                (args, target) -> { m.floodControlTime = Util.parseTime(args[0]); });
        
        addSetting("floodspan", "floodspan COUNT",
                target -> "Set flood control to apply for a user's first COUNT messages (-1 for no limit, currently " + m.floodControlSpan + ")", 1,
                (args, target) -> { m.floodControlSpan = Integer.parseInt(args[0]); });
        
        addCommand("lurktime", "lurktime TIME", "Automatically mark users as lurkers after TIME of inactivity (0 to disable)", 0,
            (args, target) -> {
                if (args.length >= 1 && args[0].matches("[\\-0-9].*")) {
                    long newTime = Util.parseTime(args[0]);
                    if (newTime == 0 || newTime >= 8 * 60000)
                        MultiUser.setLurkTime(newTime);
                }
                target.schedTell("Lurk trigger: " + Util.minSec(MultiUser.getLurkTime()));
            });

        addCommand("ban", "ban [?OPT] REGEX", "Kick or mute people opening with lines matching the regex. /!banhelp shows possible options.", 1,
                (args, target) -> m.ban(argsToString(0, args)));
        
        addCommand("banhelp", null, null, 0,
            (args, target) -> {
                target.schedTell("The ban command optionally takes switches starting with a question mark and separated by spaces before the regex.");
                target.schedTell("For a soft ban (never kick or mute): ?soft / To warn for the first transgression: ?warn");
                target.schedTell("To limit matching to a user's first N messages: ?N (?always for no limit; default is the current banspan which is " + m.banSpan + ")");
                target.schedTell("To limit to a user's first N ms spent in the room: ?tN (?forever for no limit; default is the current banspantime which is " + m.banSpanTime + ")");
                target.schedTell("For a custom mute-rather-than-kick likelihood 0.N: ?0.N (?kick or ?mute for no randomness; default is the current banmute which is " + m.muteBotFreq + ")");
                target.schedTell("?find considers matches anywhere in the string, ?lookingat only at the beginning, ?matches (default) only the whole string.");
            });
        
        addCommand("checkban", "checkban MESSAGE", "Show which ban expressions a message triggers", 1,
            (args, target) -> {
                Collection<Ban> matched = m.checkBans(argsToString(0, args));
                if (matched.isEmpty())
                    target.schedTell("No matches");
                else
                    for (Ban ban : matched)
                        target.schedTell(ban.toString());
            },
            "checkbans");

        addSetting("banspan", "banspan COUNT",
                target -> "Set bans to apply for a user's first COUNT messages (-1 for no limit, currently " + m.banSpan + ")", 1,
                (args, target) -> { m.banSpan = Integer.parseInt(args[0]); });

        addSetting("banspantime", "banspantime TIME",
                target -> "Set bans to apply for a user's first TIME in the room (-1 for no limit, currently "
                    + Util.minSec(m.banSpanTime, "no limit") + ")", 1,
                (args, target) -> { m.banSpanTime = Util.parseTime(args[0]); });

        addCommand("bans", null, "List bans", 0,
            (args, target) -> {
                StringBuilder list = new StringBuilder();
                String[] bans = Util.map(m.getBans(), Ban::toString, String[]::new);
                if (args.length > 0) {
                    String filter = argsToString(0, args);
                    bans = Util.filter(bans, ban -> ban.contains(filter));
                }
                for (String ban : bans) {
                    list.append("\nban ").append(ban);
                }
                if (list.length() > 0)
                    target.schedTell(list.substring(1));
                else
                    target.schedTell("No bans.");
            });

        addCommand("clearbans", null, "Remove all bans", 0,
                (args, target) -> m.clearBans());

        addSetting("historysize", "historysize COUNT",
                target -> "Suppress messages duplicating any of the last COUNT messages in the room (0 to disable, currently "
                    + m.getHistorySize() + ")", 1,
                (args, target) -> m.setHistorySize(Integer.parseInt(args[0])));

        addSetting("maturehuman", "maturehuman TIME",
                target -> "Disable kicks and suppress copycat messages for a user's first TIME in the room "
                    + "(-1 for no limit, currently " + Util.minSec(m.getMatureHuman(), "no limit") + ")", 1,
                (args, target) -> m.setMatureHuman(Util.parseTime(args[0])));
        
        addCommand("flashname", null, null, 1,
                (args, target) -> m.addFlashName(args[0]));
        
        addCommand("flashuser", null, null, 1,
                (args, target) -> m.toggleFlashUser(args[0]));
        
        addCommand("clearflashes", null, null, 0,
                (args, target) -> m.clearFlashes());

        addCommand("banmute", new Command("banmute FREQ", null) {
            @Override
            public void process(String[] args, MultiUser target) {
                double f = Double.parseDouble(args[0]);
                if (f >= 0 && f <= 1) {
                    m.muteBotFreq = f;
                } else {
                    target.schedTell("Must be between 0 and 1");
                }
            }

            @Override
            public String getHelpString(MultiUser target) {
                return "Set the fraction of bans to be mutes to FREQ (currently " + m.muteBotFreq + ")";
            }
        });

        addCommand("massacre", null, null, 0,
                (args, target) -> m.massacre());
                
        addCommand("killandsave", "killandsave FILE", "Halt all connections, save their states to FILE, and silently remove them", 0,
                (args, target) -> { if (target.isDummy()) m.killAndSave(argsToString(0, args)); },
                "saveandkill");

        addCommand("loadrc", "loadrc [FILE]", "Run console commands from the FILE. Beware of recursion!", 0,
                (args, target) -> loadRc(args.length > 0 ? args[0] : null, target));
        
        addCommand("nick", "nick NAME", null, 1, 
                (args, target) -> m.nick(target, argsToString(0, args)),
                "n", "name");
        
        addCommand("shinobi", "shinobi NAME", "Change your name without alerting the rest of the room", 1, 
                (args, target) -> m.nick(target, argsToString(0, args), true),
                "ninja", "silentnick", "sn");

        addCommand("deify", "deify|undeify", "Make that person a god! They can run admin commands prefixed with '/!'", 1,
                (args, target) -> m.deify(target, args[0], true),
                "d");

        addCommand("undeify", null, null, 1,
                (args, target) -> m.deify(target, args[0], false),
                "ud");
        
        addCommand("undeifyall", null, "Undeifies everyone", 0,
            (args, target) -> {
                if (!target.isDummy()) return;
                m.tellAdmin(target + " has removed all admins!");
                m.adminToken = new Object();
            });

        addCommand("challenge", null, "Set the admin password challenge (for use with the userscript)", 1,
            (args, target) -> {
                if (!target.isDummy()) return;
                m.tellAdmin(target + " has changed the admin challenge!");
                m.challenge = args[0];
            });
        
        addCommand("password", null, "Set the admin password. Log in using /password", 1,
            (args, target) -> {
                if (!target.isDummy()) return;
                m.tellAdmin(target + " has changed the admin password!");
                m.password = args[0];
            });
        
        addCommand("proxies", null, "List proxies", 0,
            (args, target) -> {
                StringBuilder list = new StringBuilder();
                ProxyConfig[] proxies = m.proxies.get();
                Arrays.sort(proxies);
                for (ProxyConfig proxy : proxies) {
                    list.append("\n").append(proxy.toString());
                    if (proxy == m.proxies.current())
                        list.append(" *");
                }
                if (list.length() > 0) {
                    target.schedTell(list.substring(1));
                }
            });
        
        addCommand("addproxy", "(add|rm)proxy HOST PORT", "Add (or remove) a SOCKS proxy to the pool", 1,
                proxyCommand(m.proxies::add));
        
        addCommand("rmproxy", null, null, 1,
                proxyCommand(m.proxies::remove), "removeproxy");
        
        addCommand("addifworking", "addifworking HOST PORT", "Add or enable a proxy, but only after checking that it works", 1,
                proxyCommand(m.proxies::addIfWorking), "addproxyifworking", "apiw");
        
        addCommand("torcontrol", "torcontrol HOST SOCKS CONTROL", "Add a Tor control port for a proxy", 3,
                (args, target) -> m.proxies.setTorControl(args[0], Integer.parseInt(args[1]), Integer.parseInt(args[2])));
        
        addCommand("useproxy", "useproxy HOST PORT", "Select an already added proxy to use (0 0 for lack of proxy)", 1,
                proxyCommand(m.proxies::use));

        addCommand("proxyoff", "proxy(off|on) H P", "Disable or enable a proxy (0 0 for lack of proxy)", 1,
                proxyCommand((host, port) -> m.proxies.setEnabled(host, port, false)));
        
        addCommand("proxyon", null, null, 1,
                proxyCommand((host, port) -> m.proxies.setEnabled(host, port, true)));
        
        addCommand("proxypriority", "proxypriority PRIO HOST PORT", 
            "Set a proxy's priority. As long as higher priority proxies are usable, lower priority ones won't be used", 2,
            (args, target) -> {
                if (!runProxyCommand(args, 1, (host, port) -> {
                    ProxyConfig proxy = m.proxies.get(host, port);
                    if (proxy == null) {
                        target.schedTell("No such proxy");
                        return;
                    }
                    proxy.setPriority(Integer.parseInt(args[0]));
                }))
                    target.schedTell("I need a priority, a host and a port");
            });
        
        addCommand("rmproxies", "(rmproxies|proxies(off|on)) REGEX", "Act on all proxies whose addresses contain a match of REGEX", 1,
                (args, target) -> m.proxies.remove(argsToString(0, args)), "removeproxies");
        
        addCommand("proxieson", null, null, 1,
                (args, target) -> m.proxies.setEnabled(argsToString(0, args), true));
        
        addCommand("proxiesoff", null, null, 1,
                (args, target) -> m.proxies.setEnabled(argsToString(0, args), false));
        
        addCommand("cleanproxies", null, "Remove all disabled proxies", 0,
                (args, target) -> m.proxies.clean(), "removedisabledproxies", "rmdisabledproxies");
        
        addSetting("proxylife", "proxylife LIFE",
                target -> "Set the time between proxy changes to LIFE (currently " + Util.minSec(m.proxies.getLife()) + ")", 1,
                (args, target) -> m.proxies.setLife(Util.parseTime(args[0])));
        
        addSetting("proxymove", "proxymove|noproxymove",
                target -> "Move sessions using open proxies to the direct connection immediately after connecting (currently "
                    + (m.moveProxies ? "on)" : "off)"), 0,
                (args, target) -> { m.moveProxies = true; },
                "moveproxies");
        
        addCommand("noproxymove", null, null, 0, (args, target) -> { m.moveProxies = false; }, "nomoveproxies", "dontmoveproxies");
        
        addCommand("logbotcmd", "logbotcmd COMMAND", "Ask LogBot for its answer to the COMMAND (need /v to see it)", 1,
            (args, target) -> {
                if (!m.logBotUserCommand(argsToString(0, args)))
                    target.schedTell("LogBot isn't here");
            });
        
        addCommand("logbotaction", "logbotaction ACTION", "Ask LogBot to do the ACTION (e.g. enablecommands)", 1,
            (args, target) -> {
                if (!m.logBotAction(argsToString(0, args)))
                    target.schedTell("LogBot isn't here");
            });
            
        addCommand("duids", "duids|huids", "Show or hide IDs before people's names", 0, 
                (args, target) -> target.showNumbers(true));
        
        addCommand("huids", null, null, 0, (args, target) -> target.showNumbers(false));
        
        sanityCheck();
    }
    
    boolean expectUser(MultiUser target, boolean success) {
        if (!success)
            target.schedTell(t("nousershort"));
        return success;
    }
    
    protected Body proxyCommand(ObjIntConsumer<String> action) {
        return (args, target) -> {
            if (!runProxyCommand(args, 0, action))
                target.schedTell("I need both a host and a port");
        };
    }
    
    static boolean runProxyCommand(String[] args, int offset, ObjIntConsumer<String> action) {
        assert args.length - offset > 0;
        String host;
        int port;
        if (args.length - offset == 1) {
            int split = args[offset].lastIndexOf(":");
            if (split == -1)
                return false;
            host = args[offset].substring(0, split);
            port = Integer.parseInt(args[offset].substring(split + 1));
        } else {
            host = args[offset];
            port = Integer.parseInt(args[offset + 1]);
        }
        action.accept(host, port);
        return true;
    }
    
    void showList(MultiUser target, boolean showTrivia, boolean showProxies) {
        StringBuilder list = new StringBuilder();
        synchronized (m.users) {
            for (MultiUser u : m.users) {
                list.append("\n " + u.getNumber()).append(". ").append(u.getNick()).append(" ");
                m.appendFlags(target, u, list);
                if (showTrivia)
                    list.append(" (").append(u.getTrivia()).append(") ");
                if (showProxies)
                    list.append(" on ").append(u.getProxy().getProxy().toString());
            }
        }
        if (list.length() > 0) {
            target.schedTell(list.substring(1));
        }
    }
    
    private static final String[] emptyArgs = {};
    
    public void adminCommand(String cmd, MultiUser target) {
        String[] keyArgs = cmd.split(" ", 2);
        String key = keyArgs[0].toLowerCase(Locale.ROOT);

        String[] args = keyArgs.length == 1 ? emptyArgs : keyArgs[1].split(" ");
        Command command = getCommand(key);
        if (command != null) {
            try {
                command.process(args, target);
            } catch (RuntimeException e) {
                StringWriter sw = new StringWriter();
                PrintWriter pw = new PrintWriter(sw);
                e.printStackTrace(pw);
                pw.flush();
                target.schedTell(sw.toString());
                // Damn you, Java, for making this so tedious
            }
        } else if (key.length() > 0) {
            target.schedTell("Unknown command! Type help for help.");
        }

    }

    void loadRc(String name, MultiUser rcuser) {
        if (name == null) {
            name = ".multirc";
        }
        try (BufferedReader file = new BufferedReader(new FileReader(name));) {
            String cmd = null;
            while ((cmd = file.readLine()) != null) {
                if (cmd.startsWith("/"))
                    m.command(rcuser, cmd);
                else if (!cmd.startsWith("#"))
                    adminCommand(cmd, rcuser);
            }
            System.gc();
        } catch (FileNotFoundException e) {
            // nop
        } catch (Exception e) {
            System.err.println("Could not load the rc file. The exception follows.");
            e.printStackTrace();
        }
    }
}
