package anon.trollegle;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodHandles.Lookup;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.WildcardType;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import static java.lang.invoke.MethodType.methodType;

class JsonSerializer<T> {
    private static UnsafeProxy unsafe = UnsafeProxy.get();
    
    private final Class<T> clbutt;
    private final Field[] fields;
    private final Map<String, Object> saveState = new LinkedHashMap<>();
    
    JsonSerializer(Class<T> clbutt) {
        this.clbutt = clbutt;
        try {
            this.fields = Util.filter(clbutt.getDeclaredFields(),
                    f -> f.getAnnotation(Persistent.class) != null);
            for (Field f : fields) f.setAccessible(true);
        } catch (Exception e) {
            if (e instanceof RuntimeException)
                throw (RuntimeException) e;
            throw new RuntimeException(e);
        }
    }
    
    boolean shouldConsider(T t, Field field) {
        return t == null && Modifier.isStatic(field.getModifiers())
            || t != null && !Modifier.isStatic(field.getModifiers());
    }
    
    final synchronized JsonValue save(T t, JsonValue previous) {
        saveState.clear();
        saveState.putAll(previous.getMap());
        saveInner(t);
        return JsonValue.wrap(saveState);
    }
    
    final synchronized JsonValue save(T t) {
        saveState.clear();
        saveInner(t);
        return JsonValue.wrap(saveState);
    }
    
    private synchronized void saveInner(T t) {
        try {
            for (Field field : fields)
                if (shouldConsider(t, field))
                    saveState.put(field.getName(), field.get(t));
        } catch (Exception e) {
            if (e instanceof RuntimeException)
                throw (RuntimeException) e;
            throw new RuntimeException(e);
        }
        customSave(t, saveState);
    }
    
    void customSave(T t, Map<String, Object> map) {}
    
    final T load(T t, JsonValue state) {
        Map<String, JsonValue> map = state.getMap();
        for (Field field : fields)
            if (shouldConsider(t, field) && map.containsKey(field.getName()))
                setField(t, field, map.get(field.getName()));
        customLoad(t, map);
        return t;
    }
    
    static Class<?> rawType(Type type, boolean upperBound) {
        Class<?> result = null;
        if (type instanceof Class)
            result = (Class<?>) type;
        else if (type instanceof ParameterizedType)
            result = rawType(((ParameterizedType) type).getRawType(), upperBound);
        else if (type instanceof WildcardType)
            if (upperBound)
                result = classyBound(((WildcardType) type).getUpperBounds(), true);
            else
                result = classyBound(((WildcardType) type).getLowerBounds(), true);
        else if (type instanceof TypeVariable)
            if (upperBound)
                result = classyBound(((TypeVariable<?>) type).getBounds(), true);
            else
                result = null;
        else if (type instanceof GenericArrayType)
            result = Array.newInstance(rawType(
                ((GenericArrayType) type).getGenericComponentType(), upperBound), 0).getClass();
        
        if (result == null)
            throw new IllegalArgumentException("rawType can't find " 
                    + (upperBound ? "upper" : "lower") + " bound for type " + type);
        return result;
    }
    
    static Class<?> classyBound(Type[] types, boolean upper) {
        Class<?> result = upper ? Object.class : null;
        for (Type bound : types) {
            result = rawType(bound, upper);
            if (!result.isInterface())
                return result;
        }
        return result;
    }
    
    @SuppressWarnings("unchecked")
    void setField(T t, Field field, JsonValue value) {
        try {
            Type genericType = field.getGenericType();
            if (genericType instanceof ParameterizedType) {
                ParameterizedType type = (ParameterizedType) genericType;
                Class<?> raw = rawType(type, true);
                Type[] params = type.getActualTypeArguments();
                if (Collection.class.isAssignableFrom(raw) && params.length == 1) {
                    Collection<?> restored = value.getAsCollectionOf(rawType(params[0], false));
                    Collection existing = (Collection) field.get(t);
                    if (existing != null) {
                        existing.clear();
                    } else {
                        existing = (Collection) raw.newInstance();
                        unsafe.putObject(field, t, existing);
                    }
                    existing.addAll(restored);
                    return;
                } else if (Map.class.isAssignableFrom(raw) && params.length == 2) {
                    Class<?> keyClass = rawType(params[0], false);
                    Class<?> valueClass = rawType(params[1], false);
                    if (keyClass == String.class) {
                        Map<String, ?> restored = value.getAsMapTo(valueClass);
                        Map existing = (Map) field.get(t);
                        if (existing != null) {
                            existing.clear();
                        } else {
                            existing = (Map) raw.newInstance();
                            unsafe.putObject(field, t, existing);
                        }
                        existing.putAll(restored);
                        return;
                    }
                }
            }
            Class<?> type = field.getType();
            if (type == int.class)
                unsafe.putInt(field, t, value.getInt());
            else if (type == double.class)
                unsafe.putDouble(field, t, value.getDouble());
            else if (type == long.class)
                unsafe.putLong(field, t, value.getLong());
            else if (type == boolean.class)
                unsafe.putBoolean(field, t, value.getBoolean());
            else if (type.isPrimitive())
                throw new IllegalArgumentException("setField doesn't support type " + type);
            else
                unsafe.putObject(field, t, value.getAs(type));
        } catch (Exception e) {
            if (e instanceof RuntimeException)
                throw (RuntimeException) e;
            throw new RuntimeException(e);
        }
    }
    
    void customLoad(T t, Map<String, JsonValue> map) {}

    Class<T> getType() { return clbutt; }
    
    @SuppressWarnings("unchecked")
    static <T> JsonSerializer<T> getFor(Class<T> type) {
        try {
            Field field = type.getDeclaredField("serializer");
            if (!Modifier.isStatic(field.getModifiers()))
                throw new UnsupportedOperationException("serializer field not static");
            if (field.getType() != JsonSerializer.class)
                throw new UnsupportedOperationException("serializer field not of type JsonSerializer");
            field.setAccessible(true);
            JsonSerializer<?> result = (JsonSerializer<?>) field.get(null);
            if (result.getType() != type)
                throw new IllegalStateException("serializer is for type " 
                        + result.getType() + "; expected " + type);
            return (JsonSerializer<T>) result;
        } catch (Exception e) {
            if (e instanceof RuntimeException)
                throw (RuntimeException) e;
            throw new RuntimeException(e);
        }
    }
}

class UnsafeProxy {
    private static UnsafeProxy instance;
    private final Object unsafe;
    private final MethodHandle putInt, putDouble, putLong, putBoolean, putObject, 
        objectFieldOffset, staticFieldOffset, staticFieldBase;
        
    static synchronized UnsafeProxy get() {
        if (instance == null) instance = new UnsafeProxy();
        return instance;
    }
    
    private UnsafeProxy() {
        try {
            Class<?> type = Class.forName("sun.misc.Unsafe");
            Constructor<?> ctor = type.getDeclaredConstructor();
            ctor.setAccessible(true);
            unsafe = ctor.newInstance();
            
            Lookup l = MethodHandles.lookup();
            putInt = l.findVirtual(type, "putInt", 
                    methodType(void.class, Object.class, long.class, int.class)).bindTo(unsafe);
            putDouble = l.findVirtual(type, "putDouble", 
                    methodType(void.class, Object.class, long.class, double.class)).bindTo(unsafe);
            putLong = l.findVirtual(type, "putLong", 
                    methodType(void.class, Object.class, long.class, long.class)).bindTo(unsafe);
            putBoolean = l.findVirtual(type, "putBoolean", 
                    methodType(void.class, Object.class, long.class, boolean.class)).bindTo(unsafe);
            putObject = l.findVirtual(type, "putObject", 
                    methodType(void.class, Object.class, long.class, Object.class)).bindTo(unsafe);
            objectFieldOffset = l.findVirtual(type, "objectFieldOffset", 
                    methodType(long.class, Field.class)).bindTo(unsafe);
            staticFieldOffset = l.findVirtual(type, "staticFieldOffset", 
                    methodType(long.class, Field.class)).bindTo(unsafe);
            staticFieldBase = l.findVirtual(type, "staticFieldBase", 
                    methodType(Object.class, Field.class)).bindTo(unsafe);
        } catch (Exception e) {
            if (e instanceof RuntimeException)
                throw (RuntimeException) e;
            throw new RuntimeException(e);
        }
    }
    
    private void pass(Throwable e) {
        if (e instanceof Error) throw (Error) e;
        if (e instanceof RuntimeException) throw (RuntimeException) e;
        if (e instanceof Exception) throw new RuntimeException(e);
        throw new Error(e);
    }
    
    private Object base(Field field, Object object) {
        try {
            if (object != null) return object;
            return staticFieldBase.invokeExact(field);
        } catch (Throwable e) {
            pass(e); return null;
        }
    }
    
    private long offset(Field field, Object object) {
        try {
            if (object != null)
                return (long) objectFieldOffset.invokeExact(field);
            return (long) staticFieldOffset.invokeExact(field);
        } catch (Throwable e) {
            pass(e); return 0;
        }
    }
    
    public void putInt(Field field, Object object, int value) {
        try { putInt.invokeExact(base(field, object), offset(field, object), value);
        } catch (Throwable e) { pass(e); }
    }
    public void putDouble(Field field, Object object, double value) {
        try { putDouble.invokeExact(base(field, object), offset(field, object), value);
        } catch (Throwable e) { pass(e); }
    }
    public void putLong(Field field, Object object, long value) {
        try { putLong.invokeExact(base(field, object), offset(field, object), value);
        } catch (Throwable e) { pass(e); }
    }
    public void putBoolean(Field field, Object object, boolean value) {
        try { putBoolean.invokeExact(base(field, object), offset(field, object), value);
        } catch (Throwable e) { pass(e); }
    }
    public void putObject(Field field, Object object, Object value) {
        try { putObject.invokeExact(base(field, object), offset(field, object), value);
        } catch (Throwable e) { pass(e); }
    }
}

@Retention(RetentionPolicy.RUNTIME)
@interface Persistent {}
